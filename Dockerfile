# Image de base
FROM node:alpine

# Dossier de travail qui servira de 
# point "d'ancrage/montage" pour les
# commandes suivantes
WORKDIR /usr/app

# copie de l'ensemble du dossier courant dans l'image
COPY ./ ./ 

# Téléchargement et installation des dépendances
RUN npm install

EXPOSE 3000

# Commande de démarrage
CMD ["node", "server.js"]
